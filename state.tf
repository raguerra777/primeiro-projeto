terraform {
  backend "s3" {
    bucket = "base-config-341299"
    key    = "gitlab-runner-fleet2"
    region = "us-east-1"
  }
}